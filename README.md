# Risser OS

> an operating system optimized for developers

## Research

### Apt Packages

- x11-xserver-utils
- imagemagick
- sway
- xwayland
- gnome-system-monitor
- iw
- pamixer
- playerctl
- mako-notifier
- qtwayland5
- gdm3
- wl-clipboard
- clipman
- lm-sensors
- slurp
- grim
- grimshot
- libglfw3-wayland
- swayidle
- swaybg
- wlogout
- light
- xdg-user-dirs
- pavucontrol
- pulseaudio
- network-manager
- nautilus
- polkitd
- pipewire
- xdg-desktop-portal-wlr
- policykit-1-gnome
- kitty
- flatpak
- snapd
- xsettingsd
- qt5ct
- lxappearance
- adwaita-icon-theme
- gnome-themes-extra
- qgnomeplatform-qt5
- qt5-style-plugins
- gnome-tweaks

### Python Packages

- bumblebee-status
- psutil
- netifaces

### Debs

- https://github.com/app-outlet/app-outlet/releases/download/v2.1.0/app-outlet_2.1.0_amd64.deb
- https://download.opensuse.org/repositories/home:/ungoogled_chromium/Debian_Bullseye/amd64/ungoogled-chromium-dbgsym_95.0.4638.54-1_amd64.deb
- https://download.opensuse.org/repositories/home:/ungoogled_chromium/Debian_Bullseye/amd64/ungoogled-chromium_95.0.4638.54-1_amd64.deb

### Missing Debian Packages

- https://github.com/nwg-piotr/autotiling
- https://github.com/lbonn/rofi (compiled for wayland)
- https://github.com/mortie/swaylock-effects
- https://github.com/nwg-piotr/nwg-displays
- https://github.com/nwg-piotr/azote

### Envs

Set the following envs before launching sway

| name | value |
| ---- | ----- |
| `__EGL_VENDOR_LIBRARY_FILENAMES` | `/usr/share/glvnd/egl_vendor.d/50_mesa.json` |
| `XDG_CURRENT_DESKTOP` | `sway` |
| `XDG_SESSION_TYPE` | `wayland` |
| `QT_QPA_PLATFORMTHEME` | `gnome` |


### Files To Patch

- _/usr/share/wayland-sessions/sway.desktop (possibly)_
- /usr/bin/light (fix permissions)
- /usr/share/applications/chromium.desktop (to fix chrome light/dark mode) `/bin/sh -c '/usr/bin/chromium $(echo $(gsettings get org.gnome.desktop.interface color-scheme) $(gsettings get org.gnome.desktop.interface gtk-theme) | grep -vq dark || echo --enable-features=WebUIDarkMode --force-dark-mode)'`

> add `_JAVA_AWT_WM_NONREPARENTING=1` to java desktop files

> add `--force-scale=1` to the end of onlyoffice desktop file

### Programs to Build

- refactor screenshot script into program and support grimshot

## Resources

- https://github.com/indicozy/wmtm
- https://gitlab.com/Jimbus/sway
- https://wiki.archlinux.org/title/Sway
- https://wiki.archlinux.org/title/Wayland
- https://wiki.archlinux.org/title/PipeWire#xdg-desktop-portal-wlr
- https://github.com/swaywm/sway/wiki
- https://github.com/swaywm/sway/wiki/GTK-3-settings-on-Wayland
